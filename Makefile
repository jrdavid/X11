cwd = $(shell pwd)
host = $(shell hostname)

install:
	ln -sf $(cwd)/Xresources $(HOME)/.Xresources
	ln -sf $(cwd)/xsession $(HOME)/.xsession
	ln -sf $(cwd)/xmodmap.$(host) $(HOME)/.xmodmap
